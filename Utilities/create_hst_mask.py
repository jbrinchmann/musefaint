#!/usr/bin/env python

#
# Create a mask for a MUSE field that indicates where there is missing data
# in an HST image.
#

import numpy as np
from astropy.io import fits
from astropy.wcs import WCS


def find_nan_in_image(im1, wcs1, im2, wcs2):
    """find_nan_in_image(im1, im2)

    Loop over all pixels in im1 and find out whether in im2 the pixel(s)
    corresponding to that position are NaN. If so, set the mask to 1.
    """


    # First we create a pixel grid for im1:
    nx, ny = im1.shape
    ix = np.arange(nx)
    iy = np.arange(ny)
    xx, yy = np.meshgrid(ix, iy, indexing='ij')
    flag = np.zeros_like(xx).flatten()
    
    # Get the Ra & Dec values for these pixels.
    ra, dec = wcs1.all_pix2world(yy.flatten(), xx.flatten(), 0)

    # Then the pixel positions in im2 with similar origin.
    y2, x2 = wcs2.all_world2pix(ra, dec, 0)
    # Round
    x2 = np.floor(x2+0.5).astype(int)
    y2 = np.floor(y2+0.5).astype(int)
    
    # The typical situation here is when im2 is an HST image,
    # so that the pixels are smaller than im1. I will check for that here. 
    pix_scale1 = wcs1.proj_plane_pixel_scales()
    pix_scale2 = wcs2.proj_plane_pixel_scales()

    scale_ratio = [(pix_scale1[0]/pix_scale2[0]).value,
                   (pix_scale1[1]/pix_scale2[1]).value]

    if scale_ratio[0] > 1:
        # im2 has a finer scale than im1
        # In this case we look at the pix location +/- scale_ratio/2.
        dx = np.floor(scale_ratio[0]/2.0+0.5).astype(int)
        dy = np.floor(scale_ratio[1]/2.0+0.5).astype(int)
        if (dx ==0):
            dx = 1
        if (dy ==0):
            dy = 1
        for i in range(len(x2)):
            subim = im2[x2[i]-dx:x2[i]+dx, y2[i]-dy:y2[i]+dy]
            if np.any(np.isnan(subim)):
                flag[i] = 1

    else:
        print("Unexpected pixel size!")
        flag[:] = -1
        
    return np.reshape(flag, (nx, ny))
            


def load_image(imfile):

    hdul = fits.open(imfile)
    if 'SCI' in hdul:
        hdu = hdul['SCI']
    else:
        hdu = hdul['DATA']

    im = hdu.data
    hdr = hdu.header
    wcs = WCS(hdr)
    
    return im, hdr, wcs



if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("musefile", type=str, help="Name of MUSE image file")
    parser.add_argument("hstfile", type=str, help="Name of HST image file")
    parser.add_argument("outfile", type=str, help="Name of output flag file")

    args = parser.parse_args()
    
    im1, hdr1, wcs1 = load_image(args.musefile)
    im2, hdr2, wcs2 = load_image(args.hstfile)

    print("Creating flag image...")
    flag = find_nan_in_image(im1, wcs1, im2, wcs2)

    
    hdu = fits.PrimaryHDU(flag)
    wcs_header = wcs1.to_header()
    hdu.header.extend(wcs_header)


    masked = np.zeros_like(im1)
    keep = np.where(flag == 1)
    masked[keep] = im1[keep]
    hdu_masked = fits.ImageHDU(masked)
    hdu_masked.header.extend(wcs_header)

    hdul = fits.HDUList()
    hdul.append(hdu)
    hdul.append(hdu_masked)

    hdul.writeto(args.outfile, overwrite=True)


    
    
