#!/usr/bin/env python

#
# Add in objects from one SExtractor catalogue from MUSE images
# into another from HST data.
#
# The script will match the photometry using the stellar sources
# and then add rows to the HST catalogue. The main filter is assumed
# to be F606W. If there are multiple filters, it is assumed that the runs
# have been made in double image mode so that the main filter ID
# works for those as well.
#


import numpy as np
from astropy.table import Table
import astropy.units as u

from astropy.coordinates import SkyCoord


def load_hst_catalogue(fname):
    """Load the HST catalogue and check that is_star exists. If not, exit"""

    t = Table().read(fname)
    if 'is_star' not in t.columns:
        print("The HST catalogue must have an is_star column. Exiting")
        exit

    return t


def load_muse_catalogue(fname):

    t = Table().read(fname)

    return t


def calibrate_muse_magnitudes(tmuse, thst, filtername,
                              maxsep=0.5*u.arcsec, maxmag=24.0):
    """Given two catalogues, match their positions and
    determine the photometry offset between the two."""


    c_muse = SkyCoord(tmuse['ALPHA_J2000'],
                      tmuse['DELTA_J2000'], frame='icrs')

    c_hst = SkyCoord(thst['ALPHA_J2000_'+filtername],
                     thst['DELTA_J2000_'+filtername],
                     frame='icrs')

    idx, d2d, d3d = c_muse.match_to_catalog_sky(c_hst)

    # I will by default be rather strict with the match. 
    keep = np.where(d2d < maxsep)

    m_muse = tmuse['MAG_BEST'][keep]
    m_hst = thst['MAG_BEST_'+filtername][idx[keep]]

    # The offset is determined in a robust manner by considering
    # only sources classified as is_star and brighter than maxmag
    diff = m_muse-m_hst
    use = np.where(thst['is_star'][idx[keep]] & (m_hst <= maxmag))

    mag_offset = np.median(diff[use])

    print("I determined a magnitude offset of {0:.2f} for {1}".format(mag_offset, filtername))

    return tmuse['MAG_BEST']-mag_offset


def _muse_columns_to_keep():
    # The columns to copy from the MUSE catalogue.
    # To avoid multiple IDs, the IDs from MUSE will have 100000 added
    # Note that the colours are calculated in the insert_extra_objects
    # routine because they operate on the combination of filters.
    
    to_copy = ['NUMBER', 'MAG_BEST', 'MAGERR_BEST', 'ALPHA_J2000',
               'DELTA_J2000', 'X_IMAGE', 'Y_IMAGE']
    return to_copy
    

def insert_extra_objects(muse_tables, ids, filternames, thst_in):
    """This inserts objects with IDs in the ids array from
    the muse_tables into thst.

    The insertion is very incomplete since there is a lack
    of agreement between the two but basically new rows are created
    for each ID and then populated in a limited way.

    It is the users responsibility to ensure that the filternames passed
    in match those in the HST file!
    """

    # Make a copy!
    thst = thst_in.copy()
                     
    # First we need to calibrate the magnitudes.
    calibrated_mags = {}
    for i, fltr in enumerate(filternames):
        tmuse = muse_tables[i]

        calibrated_mags[fltr] = calibrate_muse_magnitudes(tmuse, thst, fltr)
        
    to_copy = _muse_columns_to_keep()
        
    # Go through the IDs. Find the MUSE data, create a new row and add it.
    for idvalue in ids:
        # This step requires that the tables were created in dual image
        # model.
        tmuse_ref = muse_tables[0]
        ii, = np.where(tmuse_ref['NUMBER'] == idvalue)
        if len(ii) == 0:
            print("I was unable to find ID value {0} - please check your input!".format(idvalue))
            exit

    
        this_row = {}
        for i_f, fltr in enumerate(filternames):
            tmuse = muse_tables[i_f]
            for tag in to_copy:
                if tag == 'MAG_BEST':
                    x = calibrated_mags[fltr][ii]
                else:
                    x = tmuse[tag][ii]

                if tag == 'NUMBER':
                    # The MUSE Ids will have 100000 added to avoid duplicate IDs
                    x = x+100000
                this_row[tag+'_'+fltr] = x

            # I will also fake the MAG_PSF value as well as this is what
            # we use for Pampelmuse. This is not ideal but well, it is what
            # we have for now.
            this_row['MAG_PSF_'+fltr] = this_row['MAG_BEST_'+fltr]
                
        # With the filters inserted, we can insert colours.
        n_f = len(filternames)
        for i in range(n_f):
            f1 = filternames[i]
            for j in range(i+1, n_f):
                f2 = filternames[j]

                tag = f1+'_'+f2+'_'+'BEST'
                etag = f1+'_'+f2+'_ERR_'+'BEST'

                x = this_row['MAG_BEST_'+f1]
                y = this_row['MAG_BEST_'+f2]
                dx = this_row['MAGERR_BEST_'+f1]
                dy = this_row['MAGERR_BEST_'+f2]
                colour = x-y
                dcolour = np.sqrt(dx**2+dy**2)
                this_row[tag] = colour
                this_row[etag] = dcolour

        # The MUSE data is insufficient typically to reliable determine star or galaxy, so I will set is_star to True regardless.
        this_row['is_star'] = True
        
        # And finally we can add the row!
        thst.add_row(this_row)
                            
    return thst


def run_match(muse_files, filternames, missing_file, hst_file):

    thst = load_hst_catalogue(hst_file)
    muse_tables = []
    for mf in muse_files:
        muse_tables.append(load_muse_catalogue(mf))

    tmiss = Table().read(missing_file, format='ascii')
    ids = tmiss['Number']

    thst_updated = insert_extra_objects(muse_tables, ids, filternames, thst)

    return thst_updated


if __name__ == '__main__':

    import argparse

    # This is actually a bit complex because of the sheer number
    # of tables. Thus this is a bit of a fudge.
    
    parser = argparse.ArgumentParser()
    parser.add_argument('hst_table', type=str, help="Table with HST detections")
    parser.add_argument('missing_ids', type=str,
                        help="Table with missing IDs, must have a column named Number")
    parser.add_argument('muse_tables', type=str, nargs='+')
    parser.add_argument('--outfile', type=str, default='updated_catalogue.fits')
    parser.add_argument('--filters', type=str,
                        help="A comma separated list of filternames",
                        default="F606W,F814W")
    
    args = parser.parse_args()

    filternames = args.filters.split(',')

    if len(filternames) != len(args.muse_tables):
        print("You need to provide as many filternames as there are MUSE files!")
        exit
        
    thst = run_match(args.muse_tables, filternames, args.missing_ids, args.hst_table)

    thst.write(args.outfile, overwrite=True)


    
