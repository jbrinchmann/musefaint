#!/usr/bin/env python

from astropy.table import Table
import sys

tablename = sys.argv[1]
filtername = sys.argv[2]
outname = sys.argv[3]

print("I will parse {0} prepending {1} to all keys and write to {2}".format(tablename ,filtername, outname))

# This is made for SExtractor VOT tables
t = Table().read(tablename, table_id=0)
keys = t.colnames
for k in keys:
    new_key = filtername+'_'+k
    col = t[k]
    col.name = new_key

t.write(outname, format='votable', overwrite=True)
