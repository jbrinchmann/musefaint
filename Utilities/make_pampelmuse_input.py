#!/usr/bin/env python

# A simple script to take on HST catalogue and create an input
# catalogue for Pampelmuse.

import numpy as np
from astropy.table import Table
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('hstcat', type=str, help='Input catalogue from HST, possibly augmented')
parser.add_argument('filter', type=str, help='Which filter to use')
parser.add_argument('outcat', type=str, help='Name of output catalogue')
parser.add_argument('--maxmag', type=float, help='Faintest magnitude to consider',
                    default=25.5)
parser.add_argument('--keep_no_psf', action='store_true',
                    help='Set to keep stars without good PSF magnitude')

args = parser.parse_args()

fltr = args.filter

t = Table().read(args.hstcat)


mag = t['MAG_PSF_'+fltr]
if args.keep_no_psf:
    keep = np.where(t['is_star'])
    bad = np.where(mag > 30)
    mag[bad] = t['MAG_BEST_'+fltr][bad]
else:
    # This keeps only objects with PSF magnitude - this is rigorous
    keep = np.where(t['is_star'] & (t['MAG_PSF_'+fltr] <= args.maxmag))

t = t[keep]


tnew = Table()
tnew['id'] = t['NUMBER_'+fltr]
tnew['ra'] = t['ALPHA_J2000_'+fltr]
tnew['dec'] = t['DELTA_J2000_'+fltr]
tnew[fltr.lower()] = t['MAG_PSF_'+fltr]





tnew.write(args.outcat, format='csv')

