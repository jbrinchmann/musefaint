#!/usr/bin/env python

#
# Combine spexxy and pampelmuse data
#

from astropy.table import Table, hstack
from astropy.io import fits
import sys
import re
from os.path import join, splitext
import numpy as np
import astropy.units as u

from astropy.coordinates import SkyCoord


def load_spexxy_results(fname):

    t = Table().read(fname, format='csv')

    return t


def _get_series(hdr, pre, keys):

    x = dict()
    for k in keys:
        key = "HIERARCH "+pre+" "+k

        x[k] = hdr[key]

    return x
    

def get_single_spectrum(spfile):
    """Load one single spectrum as extracted by GETSPECTRA

    Severely lacking in error checking etc!
    """

    # Open the file
    hdul = fits.open(spfile)

    # Get the flux and flux uncertainty
    flux = hdul['PRIMARY'].data
    dflux = hdul['SIGMA'].data

    # Get the header where there is a lot of useful information.
    hdr = hdul['PRIMARY'].header

    # Create wavelength array
    l = np.arange(hdr['NAXIS1'])*hdr['CDELT1']+hdr['CRVAL1']
    

    # Insert this information into one dict for observations
    # and one for info from PampelMuse
    obs_info = dict()
    other_info = dict()

    # These all needs HIERARCH OBSERVATION before the name
    obs_keys = ['AIRM START', 'AIRM END', 'AMBI FWHM START',
                    'AMBI FWHM END', 'AMBI PRES START',
                    'AMBI RHUM', 'AMBI TAU0', 'AMBI TEMP',
                    'AMBI WINDDIR', 'AMBI WINDSP']
    for k in obs_keys:
        try:
            obs_info[k] = hdr['HIERARCH OBSERVATION '+k]
        except:
            obs_info[k] = "Missing"

    # For other keys I take a slightly uglier approach
    for k in ['ID', 'RA', 'DEC', 'MAG']:
        try:
            other_info[k] = hdr['HIERARCH STAR '+k]
        except:
            other_info[k] = "Missing"

    for k in ['MULTISTAR', 'XCUBE', 'YCUBE', 'INFIELD',
                  'EDGEDIST', 'SNRATIO', 'MAG F606W',
                  'MAG DELTA', 'MAG ACCURACY', 'TYPE', 'QLTFLAG']:
        try:
            other_info[k] = hdr['HIERARCH SPECTRUM '+k]
        except:
            other_info[k] = "Missing"


    sp = {'obsinfo': obs_info, 'info': other_info, 'flux': flux,
              'dflux': dflux, 'wave': l}

    return sp


def load_spectrum_info(fname, fltr=None):

    print("Reading "+fname)
    hdul = fits.open(fname)
    hdr = hdul[0].header
    hdul.close()

    sp = get_single_spectrum(fname)
    sn = sp['flux']/sp['dflux']
    ok = np.where(np.isfinite(sn))
    
    snr_spec = np.median(sn[ok])

    
    result = dict()
    result['MED_SNR'] = snr_spec
    
    # Check the stored type
    if "HIERARCH SPECTRUM TYPE" in hdr:
        if hdr["HIERARCH SPECTRUM TYPE"][0:3] == 'sky':
            issky=True
        else:
            issky=False
    else:
#        print("Not sure it is a sky spectrum or not!")
        issky = False

    if issky:
        y_star={"ID": -9999, "RA": -1.0, "DEC": -99, "MAG": -99.0}
        xtmp = hdr["HIERARCH SPECTRUM XCUBE"]
        ytmp = hdr["HIERARCH SPECTRUM YCUBE"]
        y_spec={"XCUBE": xtmp,
                "YCUBE": ytmp,
                "INFIELD": True,
                "EDGEDIST": np.min([float(xtmp), float(ytmp)]),
                "SNRATIO": -1.0, "TYPE": "sky", "QLTFLAG": 5}
    else:
        # I want to get rid of the ugly HIERARCH keyword name
        x_star = ["ID", "RA", "DEC", "MAG"]
        y_star = _get_series(hdr, "STAR", x_star)
        x_spec = ["XCUBE", "YCUBE", "INFIELD", "EDGEDIST",
                  "SNRATIO", "QLTFLAG"]
        y_spec = _get_series(hdr, "SPECTRUM", x_spec)
        
    result.update(y_star)
    result.update(y_spec)

    if fltr is None:
        print("You need to give the filter name!")
        return None


    x_spec_mag = [fltr, "DELTA", "ACCURACY"]
    try:
        y_spec_mag = _get_series(hdr, "SPECTRUM", x_spec_mag)
    except:
        y_spec_mag = {fltr: fltr, "DELTA": 0.0, "ACCURACY": -9999.0}
    result.update(y_spec_mag)
    
    x_spexxy = ["SUCCESS", "ERRORBARS", "NFEV", "CHISQR",
                "REDCHI", "NVARYS", "NDATA", "NFREE",
                "MSG", "SNR"]

    try:
        y_spexxy = _get_series(hdr, "ANALYSIS SPEXXY", x_spexxy)
    except:
        y_spexxy = {'SUCCESS': False, "ERRORBARS": False,
                    "NFEV": -1, "CHISQR": -1.0, "REDCHI": -1.0,
                    "NVARYS": -1, "NDATA": -1, "NFREE": -1,
                    "MSG": "N/A", "SNR": -9.}
        
    result.update(y_spexxy)

    x_an_star= ["V", "SIG", "TEFF", "LOGG", "FEH", "ALPHA"]
    try:
        y_an_star=_get_series(hdr, "ANALYSIS STAR", x_an_star)
    except:
        y_an_star = {t: -1.0 for t in x_an_star}
    
    result.update(y_an_star)

    return result


def load_prm_catalogue(prmfile):

    t = Table().read(prmfile, "SOURCES")
    return t


def load_prm_catalogue(csvfile):

    t = Table().read(csvfile, format="csv")
    return t


def load_photometry_catalogue(catname):
    """Load the photometry catalogue that is the parent for the catalogue
       used to extract spectra with PampelMuse"""

    t = Table().read(catname)
    return t


def add_tables_at_index(t1in, t2, idx):
    # idx is into t1
    t1 = t1in.copy()
    
    for tag in t2.colnames:
        # To avoid some subclassing of types
        xtmp = t2[tag].tolist()
        tp = type(xtmp[0])
#        print("I am insering {0} with type {1} into table 1".format(tag, tp))
        if tp == str:
            t1[tag] = " "*len(t2[tag][0])
            t1[tag] = "N/A"
        else:
            t1[tag] = np.NaN

        t1[tag][idx] = t2[tag]

    return t1



def match_photometry_to_table(photfile, tcat, 
                              filter='F606W'):
    """Match a file with photometry to a Spexxy/Pampelmuse table.

    The mathcing is done using sky coordinates. In this case
    we know that the Pampelmuse input came from this HST
    catalogue so there is no need to cut on maximum distance. 
    """

    tphot = load_photometry_catalogue(photfile)

    #
    # We will match on sky because the matching on ID number
    # fails too easily.
    #
    c = SkyCoord(tcat['RA']*u.degree, tcat['DEC']*u.degree, frame='icrs')
    cat = SkyCoord(tphot['ALPHA_J2000_'+filter], tphot['DELTA_J2000_'+filter],
                   frame='icrs')
    idx, d2d, d3d = c.match_to_catalog_sky(cat)

    #
    # Finally, we add all columns into the main table (the photometry file) 
    #
    tfinal = add_tables_at_index(tphot, tcat, idx)

    return tfinal

    

def add_columns_to_spexxy(t, fltr, ROOT="."):
    """Add columns to the spexxy results by loading info from the FITS files"""


    h = dict()
    first = True

    n_stars = len(t)
    
    for row in t:
        fname = join(ROOT, row["Filename"])

        r = load_spectrum_info(fname, fltr=fltr)

        if first:
            print(" I will make a struct with the following keys=", list(r.keys()))
            for k in r.keys():
                h[k] = []
            first = False

        for k in r.keys():
            h[k].append(r[k])

    t_tmp = Table(h)

    t_new = t.copy()
    for k in t_tmp.columns:
        t_new.add_column(t_tmp[k])


    return t_new



# Create the script.
if __name__ == '__main__':
    

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("spexxyfile", type=str, help="CSV file with spexxy results")
    parser.add_argument("prmfile", help="Pampelmuse PRM file", type=str)
    parser.add_argument("photfile", help="Photometry master file", type=str)
    parser.add_argument("--filter", default="F606W", type=str,
                        help="Filter used for the Pamplemuse run")
    parser.add_argument("--root", default="./", type=str,
                        help="Root directory for the extracted Pampelmuse spectra")
    parser.add_argument("--outfile", default="combined_table.fits", type=str,
                        help="Filter used for the Pamplemuse run")

    
    args = parser.parse_args()

    spexxy = load_spexxy_results(args.spexxyfile)
    spexxy = add_columns_to_spexxy(spexxy, args.filter, ROOT=args.root)

    # Then create a matched table.
    tmatch = match_photometry_to_table(args.photfile, spexxy)


    # And finally get only thus run through spexxy.
    good = np.where(tmatch["ID"] >= 0)
    tspexxy = tmatch[good]

    outname1 = args.outfile
    start, ext = splitext(outname1)
    outname2 = start+'-spexxy'+ext
    
    
    tmatch.write(outname1, overwrite=True)
    tspexxy.write(outname2, overwrite=True)
    
