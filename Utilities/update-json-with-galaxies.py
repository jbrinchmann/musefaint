import sys

from astropy import table
import json

catalogfile = sys.argv[1]
configfiles = sys.argv[2:]

catalog = table.Table.read(catalogfile)
# Update.
ids = [int(i) for i in catalog[catalog['id']>=1000000]['id']]

if len(ids) > 0:
    for configfile in configfiles:
        with open(configfile, 'r+') as f:
            config = json.load(f)
            config['catalog']['saturated'] = ids
            f.seek(0)
            json.dump(config, f, indent=4)
