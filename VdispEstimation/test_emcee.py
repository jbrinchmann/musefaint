import ufd_emcee_models as um
import numpy as np

# This should be moved into a test suite.


# Make a model
m = um.UFDEmceeModel('Gaussian')

# Calculate a trivial likelihood
theta = [0.0, 1.0]

m = um.UFDEmceeModel('Gaussian')

calculated = m.lnprob(theta, np.array([1.0]), np.array([1.0]))

# The true value here is
#   sigma^2 = 1.0^2 + 1.0^2 = 2 -> sigma=sqrt(2)
#
#   exp(-1/2*sigma2)/sqrt(2*pi*sigma2) = exp(-1/4)/sqrt(4 pi)
#
# so ln L becomes
#
#   -1/4 - 0.5 ln (4 pi)
true_value = -0.25 - 0.5*np.log(4*np.pi)

if (np.abs(calculated-true_value) > 1e-8):
    print("Problem!!!")
else:
    print("All ok!")


