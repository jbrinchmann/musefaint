### OUTDATED USE ufd_pymc_models.py INSTEAD!!!


from astropy.table import Table
import numpy as np
import matplotlib.pyplot as plt
#import pymc3 as pm  
import pymc as pm  # Using the newer version
import arviz as az
import scipy.special as ss
import scipy.stats




def fit_to_gaussian(w, dw, sample=2000, tune=1500, noscale=False):

    Ntotal = len(w)

    with pm.Model() as model:
        # Draw the underlying parameters
        mu = pm.Normal("mu", 300.0, 20)  # Mean velocity of the UFD
        sigmav = pm.Uniform("sigmav", lower=0, upper=30.0)   # V. disp of UFD


        # Draw the true velocity - this draws for all possible stars
        v = pm.Normal("v", mu=mu, tau=1.0/sigmav**2, shape=Ntotal)

        if noscale:
            scl_ki = 1.0
        else:
            # And the scaling factor for the Kirby et al uncertainties - we multiply the Kirby uncertainties by this.
            scl_ki = pm.Uniform("scl_ki", lower=0.3, upper=2.5)

        # Then the observed velocity - first the MUSE ones, then Kirby et al
        vobs = pm.Normal("vobs_muse", mu=v[inds_muse], tau=1.0/dw_spexxy[inds_muse]**2,
                            observed=w_spexxy[inds_muse], shape=len(inds_muse))

        sigma_scaled = dw_kirby[inds_kirby]*scl_ki
        vobs = pm.Normal("vobs_kirby", mu=v[inds_kirby], tau=1.0/sigma_scaled**2,
                            observed=w_kirby[inds_kirby], shape=len(inds_kirby))
            
            

        idata = pm.sample(sample, tune=tune, return_inferencedata=True, chains=2)

    return model, idata



def fit_to_gaussian_multisample(t, inds_muse, inds_kirby, sample=2000, tune=1500, noscale=False):


    print("THIS HAS NOT BEEN UPDATED YET!!!")
    return None

    # Define the vario us velocities we use below
    
    w = t['RVel'].data
    dw = t['e_RVel'].data

    w_kirby = t['Kirby.v_helio'].data.data
    dw_kirby = t['Kirby.v_helio_err'].data.data

    w_spexxy = t['spexxy.STAR_V'].data.data
    dw_spexxy = t['spexxy.STAR_V_ERR'].data.data

    
    Ntotal = len(w)

    with pm.Model() as model:


        # Draw the underlying parameters
        mu = pm.Normal("mu", 300.0, 20)  # Mean velocity of the UFD
        sigmav = pm.Uniform("sigmav", lower=0, upper=30.0)   # V. disp of UFD


        # Draw the true velocity - this draws for all possible stars
        v = pm.Normal("v", mu=mu, tau=1.0/sigmav**2, shape=Ntotal)

        if noscale:
            scl_ki = 1.0
        else:
            # And the scaling factor for the Kirby et al uncertainties - we multiply the Kirby uncertainties by this.
            scl_ki = pm.Uniform("scl_ki", lower=0.3, upper=2.5)

        # Then the observed velocity - first the MUSE ones, then Kirby et al
        vobs = pm.Normal("vobs_muse", mu=v[inds_muse], tau=1.0/dw_spexxy[inds_muse]**2,
                            observed=w_spexxy[inds_muse], shape=len(inds_muse))

        sigma_scaled = dw_kirby[inds_kirby]*scl_ki
        vobs = pm.Normal("vobs_kirby", mu=v[inds_kirby], tau=1.0/sigma_scaled**2,
                            observed=w_kirby[inds_kirby], shape=len(inds_kirby))
            
            

        idata = pm.sample(sample, tune=tune, return_inferencedata=True, chains=2)

    return model, idata
