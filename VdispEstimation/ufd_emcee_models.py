import numpy as np
import emcee

#
# As there is likely little commonality between other MCMC or
# sampling methods and emcee in the model formulation I will
# not create a superclass for this.
#

#
# Type - specifies the general model family. 
#
#  Gaussian     - simple Gaussian, no background
#  GaussUnifBg  - Gaussian with a uniform background
#  GaussGaussBg - Gaussian with a Gaussian background
#
# PriorType - specifies the prior type.
#
#  Uniform     - Standard uniform prior between limits.
#  Gaussian    - Gaussian prior
#

class UFDEmceeModel:

    defaults = {
        'vmin': -500.0,
        'vmax': 500.0,
        'sigmamin': 0.0,
        'sigmamax': 30.0,
        'fracmin': 0.0,
        'fracmax': 1.0,
        'bgvmin': -500.0,
        'bgvmax': 500.0,
        'bgvmean': 0.0,
        'bgvsigma': 100.0
        }
        
        
    
    def __init__(self, modeltype, priortype="Uniform", priorargs={}):
        """Initialise a UFDEmceeModel object. This sets the type of model 
        and also sets the prior parameters. The latter is very flexible but
        for that reason also a bit opaque.
        """

        self.type = modeltype
        self.priortype = priortype

        # We need to set the prior argument defaults
        # This is just done automatically with no checking of whether the
        # prior values make sense!
        p = self.set_prior_defaults(priorargs)
        self.priorargs = p
        
        #
        # We next need to define likelihoods depending on the model chosen.
        # This is handled by creating functions within the function calls below. 
        #
        self.lnprob = self.build_posterior_functions()

        
        

    def set_prior_defaults(self, p):
        """Set any prior defaults that are not specified by
        the user. 
        """

        for c in self.defaults.keys():
            if c not in p:
                p[c] = self.defaults[c]
        
        return p 


    def build_posterior_functions(self):
        """Build the posterior function that emcee will call.
        
        This cannot contain self an argument so will be built up using 
        closures"""

        ln_L = self.build_likelihood()
        ln_prior = self.build_prior()

        # Build the closure
        def lnprob(theta, v, verr):

            lp = ln_prior(theta)
            logL = ln_L(theta, v, verr)
            if not np.isfinite(lp) or not np.isfinite(logL):
                return -np.inf
            else:
                return lp+logL

        return lnprob

        

    def build_likelihood(self):
        """Build the likelihood portion of the posterior.
        
        """

        # This is model dependent and will be broken down in sub-functions

        if self.type == 'Gaussian':
            ln_L = self.build_likelihood_gaussian(background=None)
        elif self.type == "GaussianUnifBg":
            ln_L = self.build_likelihood_gaussian(background="Uniform")
        elif self.type == "GaussGaussBg":
            ln_L = self.build_likelihood_gaussian(background="Gaussian")

        return ln_L


    def build_likelihood_gaussian(self, background=None):
        """Build a Gaussian likelihood model for the UFD"""

        UNIF_NORM = 1.0/(self.priorargs["bgvmax"]-self.priorargs["bgvmin"])
        VMID = self.priorargs["bgvmean"]
        SIGBG = self.priorargs["bgvsigma"]
        NORM = (1.0/(np.sqrt(2*np.pi)*SIGBG))
        
        def _background(v):
            if background is None:
                return 0
            elif background == "Uniform":
                return UNIF_NORM
            elif background == "Gaussian":
                z2_bg = 0.5*((v-VMID)/SIGBG)**2
                return NORM*np.exp(-z2_bg)

    
        def lnL_func(theta, v, verr):
            """The marginalised likelihood

            The order of the variables in theta is
               theta[0] = mean velocity
               theta[1] = intrinsic velocity dispersion
               theta[2] = fraction belonging to the background
               theta[3]... [Model dependent]
            """
            mu_ufd = theta[0]
            sigma_ufd = theta[1]

            
            # I could save some time by passing in verr^2 and using sigma_ufd^2 as 
            # my parameter. I'll keep it like this for now for clarity.
            sigma2 = verr**2+sigma_ufd**2
            z = (v-mu_ufd)
            z2 = 0.5*z**2/sigma2
    
            # The Gaussian part of the likelihood
            g = (1.0/np.sqrt(2*np.pi*sigma2))*np.exp(-z2)

            if background is None:
                lnL = np.sum(np.log(g))
            else:
                p_bg = _background(v)
                f = theta[2]
                L = f*g + (1-f)*p_bg
                lnL = np.sum(np.log(L))
            

            return lnL


        return lnL_func


    def build_prior(self):
        """Define a prior function."""

        
        if (self.priortype == "Uniform"):
            if (self.type == "Gaussian"):
                ln_prior = self.build_uni_gaussian_prior()
            elif (self.type == "GaussianUnifBg"):
                ln_prior = self.build_uni_gaussianunifbg_prior()
        else:
            print("Unknown prior!!")
            exit

        self.ln_prior = ln_prior

        return ln_prior
        

    def build_uni_gaussian_prior(self):

        VMIN = self.priorargs["vmin"]
        VMAX = self.priorargs["vmax"]
        SMIN = self.priorargs["sigmamin"]
        SMAX = self.priorargs["sigmamax"]
        
        def lnprior(theta):
            mu_ufd, sigma_ufd = theta
            
            if ((VMIN < mu_ufd < VMAX) and  (SMIN < sigma_ufd < SMAX)):
                lnp = 0.0
            else:
                lnp = -np.inf
            
            return lnp


        return lnprior



    def build_uni_gaussianunifbg_prior(self):

        VMIN = self.priorargs["vmin"]
        VMAX = self.priorargs["vmax"]
        SMIN = self.priorargs["sigmamin"]
        SMAX = self.priorargs["sigmamax"]
        
        def lnprior(theta):
            mu_ufd, sigma_ufd, p_ufd = theta
            
            if ((VMIN < mu_ufd < VMAX) and
                (SMIN < sigma_ufd < SMAX) and
                (0.0 <= p_ufd <= 1.0)):
                lnp = 0.0
            else:
                lnp = -np.inf
            
            return lnp


        return lnprior

