# Velocity dispersion estimation scripts

You can find here code to estimate velocity dispersions in ultra-faint
dwarfs. There is code for two different estimators: `pymc` and
`emcee`. They are different in philosophy and depending on the
situation one is better than the other.

The `ufd_emcee_models.py` file contains various convenience functions
for setting up an emcee model, while `ufd_pymc_models.py` does the
same for pymc. 

The `sigma_estimator_emcee.py` is a script using the
`ufd_emcee_models.py` set of routines to estimate the velocity
dispersion for a simple case but note that this requires tha the input
catalogue is pre-filtered.
