"""
An object for running pymc models on ultra-faint dwarf data.
"""

from astropy.table import Table
import numpy as np
import matplotlib.pyplot as plt
#import pymc3 as pm  
import pymc as pm  # Using the newer version
import arviz as az
import scipy.special as ss
import scipy.stats


class UFDPymcModel:

    defaults = {
        'vmin': -500.0,
        'vmax': 500.0,
        'vmean': 0.0,
        'sigma_vmean': 20.0,
        'sigmamin': 0.0,
        'sigmamax': 30.0,
        'fracmin': 0.0,
        'fracmax': 1.0,
        'bgvmin': -500.0,
        'bgvmax': 500.0,
        'bgvmean': 0.0,
        'bgvsigma': 100.0
    }

    def __init__(self, modeltype, priortype="Uniform", priorargs={}):
        """Initialise a UFDEmceeModel object. This sets the type of model 
        and also sets the prior parameters. The latter is very flexible but
        for that reason also a bit opaque.
        """

        self.type = modeltype
        self.priortype = priortype

        # We need to set the prior argument defaults
        # This is just done automatically with no checking of whether the
        # prior values make sense!
        p = self.set_prior_defaults(priorargs)
        self.priorargs = p


    def set_prior_defaults(self, p):
        """Set any prior defaults that are not specified by
        the user. 
        """

        for c in self.defaults.keys():
            if c not in p:
                p[c] = self.defaults[c]
        
        return p 


    def fit_single_gaussian(self, w, dw, sample=2000, tune=1500, noscale=False, chains=2):
        """Fit a single population with a simple Gaussian model"""

        Ntotal = len(w)

        with pm.Model() as model:
            #
            # Draw the underlying parameters
            #
            mu = pm.Normal("mu", self.defaults["vmean"], self.defaults["sigma_vmean"])  # Mean velocity of the UFD
            sigmav = pm.Uniform("sigmav", lower=self.defaults["sigmamin"],
                                upper=self.defaults["sigmamax"])   # V. disp of UFD


            #
            # Draw the true velocity - this draws for all possible stars
            #
            v = pm.Normal("v", mu=mu, tau=1.0/sigmav**2, shape=Ntotal)

            #
            # Then the observed velocity - first the MUSE ones, then Kirby et al
            #
            vobs = pm.Normal("vobs_muse", mu=v, tau=1.0/dw**2,
                             observed=w, shape=Ntotal)


            idata = pm.sample(sample, tune=tune, return_inferencedata=True, chains=chains)

        return model, idata




    def fipyt_single_gaussian(self, w, dw, sample=2000, tune=1500, noscale=False, chains=2):
        """Fit a single population with a simple Gaussian model"""

        Ntotal = len(w)

        with pm.Model() as model:
            #
            # Draw the underlying parameters
            #
            mu = pm.Normal("mu", self.defaults["vmean"], self.defaults["sigma_vmean"])  # Mean velocity of the UFD
            sigmav = pm.Uniform("sigmav", lower=self.defaults["sigmamin"],
                                upper=self.defaults["sigmamax"])   # V. disp of UFD


            #
            # Draw the true velocity - this draws for all possible stars
            #
            v = pm.Normal("v", mu=mu, tau=1.0/sigmav**2, shape=Ntotal)

            #
            # Then the observed velocity - first the MUSE ones, then Kirby et al
            #
            vobs = pm.Normal("vobs_muse", mu=v, tau=1.0/dw**2,
                             observed=w, shape=Ntotal)


            idata = pm.sample(sample, tune=tune, return_inferencedata=True, chains=chains)

        return model, idata

    

    def calculate_summaries(self, idata):

        df = az.summary(idata, var_names=["mu", "sigmav"],
                        stat_funcs={"std": np.std,
                                    "5%": lambda x: np.percentile(x, 5),
                                    "16%": lambda x: np.percentile(x, 16.0),
                                    "median": np.median,
                                    "84%": lambda x: np.percentile(x, 84.0),
                                    "95%": lambda x: np.percentile(x, 95),
                                    }, extend=False)

        return df
    

    def show_corner_plot(self, idata):

        az.plot_pair(idata, var_names=["mu", "sigmav"], kind=["kde"],
                     kde_kwargs={"fill_last": False}, marginals=True,
                     point_estimate="mode")
