#!/usr/bin/env python

#
# Fit the various velocity sets with emcee.
#

import numpy as np
import matplotlib.pyplot as plt
from astropy.table import Table, join
import emcee
import corner
import os.path
import os
import shutil
import argparse

import ufd_emcee_models as ufd_models


def run_fit(v, verr, model, init=[100.0, 10.0], backend=None, 
            Nwalkers=100, n_steps=901,
            autocorrelation_limit=None, background=False):
    """Run a fit for a model - this needs work! And it should probably be integrated into the package"""

    theta_init = init


    if background:
        
        ndim = 3
    else:
        ndim = 2

        
    pos = []
    for i in range(Nwalkers):
        # Create random starting positions
        p_tmp = theta_init.copy()
        # The mean velocity
        p_tmp[0] = (p_tmp[0] + 1*np.random.randn(1))[0]
        # The velocity dispersion
        p_tmp[1] = p_tmp[1] + 1*np.random.uniform(low=-0.9, high=0.9)
        # And optionally the membership
        if background:
            p_tmp[2] = p_tmp[2] + 0.1*np.random.uniform(low=0.0, high=1.0)

        pos.append(p_tmp)

    if backend is not None:
        bk = emcee.backends.HDFBackend(backend)
        bk.reset(Nwalkers, ndim)
    else:
        bk = None
        
    sampler = emcee.EnsembleSampler(Nwalkers, ndim, model.lnprob, args=(v, verr), backend=bk)
    
    
    if autocorrelation_limit is None:
        # In this case we do not monitor the autocorrelation time and just call run_mcmc
        res = sampler.run_mcmc(pos,  n_steps)
        return res, sampler
    else:
        # Monitor, following the emcee tutorial documentation
        max_n = 100000

        # We'll track how the average autocorrelation time estimate changes
        index = 0
        autocorr = np.empty(max_n)

        # This will be useful to testing convergence
        old_tau = np.inf

        # Now we'll sample for up to max_n steps
        for sample in sampler.sample(coords, iterations=max_n, progress=True):
            # Only check convergence every 100 steps
            if sampler.iteration % 100:
                continue

            # Compute the autocorrelation time so far
            # Using tol=0 means that we'll always get an estimate even
            # if it isn't trustworthy
            tau = sampler.get_autocorr_time(tol=0)
            autocorr[index] = np.mean(tau)
            index += 1

            # Check convergence
            converged = np.all(tau * 100 < sampler.iteration)
            converged &= np.all(np.abs(old_tau - tau) / tau < 0.01)
            if converged:
                break
            old_tau = tau
        
        return converged, sampler, autocorr, index
        



# Visualisation routines

def _get_labels(n_vars):
    labels = [r'$\mu$', r'$\sigma$']
    if (n_vars == 3):
        labels.append(r'$f_{UFD}$')
        
    return labels

def show_chains(sampler, ax=None):
    chain = sampler.chain
    nx, ny, nz = chain.shape
    
    labels = _get_labels(nz)        
    n_todo = len(labels)

    for i_dim in range(n_todo):
        plt.subplot(n_todo,1,i_dim+1)
        plt.ylabel(labels[i_dim])
        for i in range(nx):
            plt.plot(chain[i,:,i_dim],color='black', alpha=0.3)
        
    fig = plt.gcf()
    fig.set_size_inches(12, 10)

    return fig
    
def show_triangle(sampler, n_burn=50):
    chain = sampler.chain
    nx, ny, nz = chain.shape
    samples = chain[:, n_burn:, :].reshape((-1, nz))
    labels = _get_labels(nz)        
    n_todo = len(labels)

    print("Length of labels: {0}".format(n_todo))
    print("Labels = ", labels)
    
    fig = corner.corner(samples[:, 0:n_todo], labels=labels,
                    quantiles=None, show_titles=True, smooth=0.5)

    return fig
    


def calculate_summaries(sampler, n_burn=50):

    chain = sampler.chain
    nx, ny, nz = chain.shape
    samples = chain[:, n_burn:, :].reshape((-1, nz))

    vlos = np.median(samples[:, 0])
    sigma = np.median(samples[:, 1])

    qs = [0.025, 0.16, 0.5, 0.84, 0.975]
    vlos_q = np.quantile(samples[:, 0], qs)
    sigma_q = np.quantile(samples[:, 1], qs)
    
    result = {'quantiles': qs,
              'vlos': vlos_q,
              'sigma_int': sigma_q}

    if nz == 3:
        # In this case we have a fraction as well.
        frac_q = np.quantile(samples[:, 2], qs)
        result['fraction'] = frac_q

    return result



def test_running():
    # A simple test to check that all works fine.

    v_mean = 100.0
    s_int = 10.0
    N_stars = 100
    vtrue = np.random.normal(loc=v_mean, scale=s_int, size=N_stars)
    dv = 1.0 # km/s
    vobs = vtrue + np.random.normal(loc=0.0, scale=dv, size=N_stars)
    dvobs = dv+np.zeros(N_stars)
    
    # Create a model.
    p_args= {'vmin': 0, 'vmax': 200}
    m = ufd_models.UFDEmceeModel('Gaussian', priortype='Uniform', priorargs=p_args)
    
    res, sampler = run_fit(vobs, dvobs, m, init=[95.0, 11.0])
    
    r = calculate_summaries(sampler, n_burn=50)
    print("True:  <v>={0:7.3f}   <sigma>={1:6.4f}".format(v_mean, s_int))
    print("MCMC:  <v>={0:7.3f}   <sigma>={1:6.4f}".format(r['vlos'][2], r['sigma_int'][2]))
    
    return res, sampler

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Parse a catalogue with velocities to get sigma and vlos")
    parser.add_argument("catalogue", help="Name of catalogue to read")
    parser.add_argument("output_dir", help="Directory to store results to. If it exists, the script will stop.")
    parser.add_argument("--vmin", type=float, default=-200,
                        help="Minimum velocity for the prior")
    parser.add_argument("--vmax", type=float, default=300,
                        help="Maximum velocity for the prior")
    parser.add_argument("--select", action='store_true')
    

    args = parser.parse_args()

    # Create the output directory
    try:
        os.mkdir(args.output_dir)
    except:
        print("The output directory exists. Please remove and try again")
        exit()
    
    # Load the necessary table
    print("Reading the table from {0}".format(args.catalogue))
    t = Table().read(args.catalogue)

    # The catalogue should already be pre-selected but if the select option is set then
    # we can do some rudimentary selection here.
    if args.select:
        print("The automatic selection might be quite sub-optimal!")
        print("Not yet implemented!")
        
    try:
        v = t['STAR V']
        dv = t['STAR V ERR']
    except:
        print("Failure to read columns!")
        print(t.colnames)

        exit()

    med = np.median(v)
    mad = np.median(np.abs(v-med))
    
    init_guess = [np.median(v), mad, 0.9]
                  
    

    # Make the model - we use a Gaussian with a uniform background as our default model. 
    p_args = {'vmin': args.vmin, 'vmax': args.vmax}
    m =  ufd_models.UFDEmceeModel('GaussianUnifBg', priorargs=p_args,
                                  priortype='Uniform')


    h5_outfile = os.path.join(args.output_dir, "chains.h5")
    res, sampler = run_fit(v, dv, m, init=init_guess, backend=h5_outfile)

    stats = calculate_summaries(sampler)

    # And create some handy plots.
    chain_plot = os.path.join(args.output_dir, "chains.png")
    fig = show_chains(sampler)
    plt.savefig(chain_plot)

    triangle_plot = os.path.join(args.output_dir, "triangle.pdf")
    fig = show_triangle(sampler)
    plt.savefig(triangle_plot)
    
    

    
