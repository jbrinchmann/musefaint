#!/usr/bin/env pythong

"""make_star_selection.py

A small script to find stars in a joined photometry catalogue produced using PSFEx
"""

from astropy.table import Table
import numpy as np
import argparse
import star_selection as st



parser = argparse.ArgumentParser()
parser.add_argument('file', help="Name of joined catalogue")
parser.add_argument('outfile', help="Name of output file")
parser.add_argument('--maxmag', help="The faintest magnitude to use", type=float,
                    default=24.5)
parser.add_argument('--minmag', help="The faintest magnitude to use", type=float,
                    default=15.0)
parser.add_argument('--spreadcut', help='The cut to use of SPREAD_MODEL', default=0.01)
parser.add_argument('--spread_offset', help='A value to subtract off SPREAD_MODEL', type=float, default=0.00)
parser.add_argument('--add_ids', help="File with additional IDs to include")
parser.add_argument('--filter', help="Which filter to get information from", default="F606W")

args = parser.parse_args()


settings = {"file": args.file, "outfile": args.outfile,
            "keep": args.add_ids, "spreadcut": args.spreadcut,
            "minmag": args.minmag, "maxmag": args.maxmag,
            "filter": args.filter, "verbose": 1, 'spread_offset': args.spread_offset}
st.run_star_selection(settings)

