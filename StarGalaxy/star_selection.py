# Utilities for selecting stars

from astropy.table import Table
from astropy.io.ascii.core import InconsistentTableError
import numpy as np


def load_ids(fname):
    try:
        t = Table().read(fname, format='ascii.commented_header')
        ids = t["IDs"].data
    except InconsistentTableError:
        # Backup solution.
        t = Table().read(fname, format='ascii.no_header')
        ids = t["col1"].data
    except:
        raise

    return ids


def load_table(fname):
    """Load the photometry table
    """
    t = Table().read(fname)

    return t


def get_column(t, what, filtername):

    return t[what+"_"+filtername]

def calculate_adjusted_uncertainty(t, fltr, kappa=4.0, epsilon=5e-3):
    """Calculate adjusted uncertainties as suggested in

    https://sextractor.readthedocs.io/en/latest/Model.html#model-based-star-galaxy-separation-spread-model

    The default epsilon seems reasonable in general. 
    """

    sp_err = get_column(t, "SPREADERR_MODEL", fltr)

    dsp = np.sqrt(epsilon**2 + (kappa*sp_err)**2)

    return dsp

def find_stars(t, settings):
    """find stars accordingto a simple algorithm"""

    # i will need this frequenctly so define it here.
    fltr = settings["filter"]
    dspread = calculate_adjusted_uncertainty(t, fltr)

    spread = get_column(t, "SPREAD_MODEL", fltr)-settings['spread_offset']

    

    
    mag = get_column(t, "MAG_PSF", fltr)
    
    i_stars = np.where((np.abs(spread) <= dspread) & (mag <= settings['maxmag']) &
                       (mag >= settings["minmag"]))

    if settings["verbose"] > 0:
        print("We had a total of {0} matches".format(len(i_stars[0]) ))
    
    return list(i_stars[0])



def _check_settings(s):
    """Check that all keys are present and add if necessary"""

    required = ["file", "outfile", "spreadcut", "minmag", "maxmag", "filter", "spread_offset"]
    optional = ["keep", "remove", "verbose"]

    failure = False
    for r in required:
        if r not in s:
            print("The key {0} is missing from the settings...".format(r))
            failure = True

    for o in optional:
        if o not in s:
            if o == "verbose":
                s[o] = 0
            else:
                s[o] = None

    if failure:
        print("Unable to continue!")
        exit()

    return
    
def run_star_selection(settings):

    _check_settings(settings)
    
    t = load_table(settings["file"])

    # First find stars the straightforward way.
    indx = find_stars(t, settings)

    # Then, _if_ we have IDs, go through the rest of the list.
    id_ref = get_column(t, "NUMBER", settings["filter"])

    # Find the additional stars and add them.
    if settings["keep"] is not None:
        to_keep_ids = load_ids(settings["keep"])
        for i in to_keep_ids:
            ii,  = np.where(id_ref == i)
            if len(ii) > 0:
                indx.append(ii[0])


    # Also remove stars if that is requested
    if settings["remove"] is not None:
        to_remove_ids = list(load_ids(settings["remove"]))

        for i in to_remove_ids:
            ii,  = np.where(id_ref == i)
            if len(ii) > 0:
                try:
                    indx.remove(ii[0])
                except:
                    pass




    is_star = np.zeros(len(t), dtype="bool")
    is_star[indx] = True
    t["is_star"] = is_star

    t.write(settings["outfile"], overwrite=True)

            
    

    
    
