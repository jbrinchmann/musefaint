#!/usr/bin/env python

"""
This routine takes a table where two tables have been joined and 
renames the _1 and _2 columns to have filters. It also calculates 
some basic colours.
"""

import argparse
from astropy.table import Table
import numpy as np

# Insert colours.
def insert_colours(t, filters, mags=['MAG_AUTO', 'MAG_PETRO', 'MAG_APER', 'MAG_BEST']):
    """Calculate the various colours for the set of filters"""

    tout = t
    n_f = len(filters)
    for i in range(n_f):
        f1 = filters[i]
        for j in range(i+1, n_f):
            f2 = filters[j]
            for m in mags:
                # This assumes the standard naming in SExtractor output
                merr = 'MAGERR'+m[3:]
                suffix = m[4:]
                
                # Form this colour.
                x = t[m+'_'+f1]
                y = t[m+'_'+f2]
                colour = x-y
                key = f1+'_'+f2+'_'+suffix
                print("Inserting {0} as colour".format(key))
                tout[key] = colour

                # And then the uncertainty
                dx = t[merr+'_'+f1]
                dy = t[merr+'_'+f2]
                dcolour = np.sqrt(x**2-y**2)
                key = f1+'_'+f2+'_ERR_'+suffix
                tout[key] = dcolour
                

    return tout


parser = argparse.ArgumentParser()
parser.add_argument('table', help="FITS or VOT table with matched data")
parser.add_argument('filter1', help="First filter - corresponding to _1")
parser.add_argument('filter2', help="Second filter - corresponding to _2")

args = parser.parse_args()

# Load the table.
t = Table().read(args.table)
# Assemble the filters
filters = [args.filter1, args.filter2]

# Change the columns
columns = t.colnames

for c in columns:
    if c[-2:] == '_1':
        t.rename_column(c, c[:-1]+filters[0])
    elif c[-2:] == '_2':
        t.rename_column(c, c[:-1]+filters[1])

# Insert colours
t = insert_colours(t, filters, mags=['MAG_AUTO', 'MAG_PETRO',
                                         'MAG_APER', 'MAG_BEST', 'MAG_PSF'])

# This could be fixed to handle things in directories.
outfile = 'tidied-'+args.table
t.write(outfile, format='fits', overwrite=True)
