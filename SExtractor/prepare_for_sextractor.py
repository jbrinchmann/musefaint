#!/usr/bin/env python

"""Take an HST image and create files suitable 
for running SExtractor"""

import sys
from astropy.io import fits
import numpy as np
import setup_sextractor as ss
from scipy.ndimage import binary_dilation

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('file', help="Name of the HST image file to parse")
parser.add_argument('--psfex', action="store_true",
                        help="Create setup files so that the output can be used with PSFex")
parser.add_argument('--withpsf', action="store_true",
                        help="Create setup files for running with PSF from PSFex. Normally used with --noimage")

parser.add_argument('--noimage', action="store_true",
                        help="Do not create images - this is used when rerunning the code but where the image has already been created.")

args = parser.parse_args()

# Get the filename
filename = args.file

# Open the FITS file and get header information
hdul = fits.open(filename)
primary_header = hdul[0].header
image_header = hdul['SCI'].header

if not args.noimage:

    # We need to calculate an equivalent RMS map for SExtractor.
    # this seems to be unclear how to do as different people give
    # different suggestions. I am going with Roy Gal's suggestion
    # at http://www.ifa.hawaii.edu/~rgal/science/sextractor_notes.html
    whdu = hdul['WHT'].copy()

    rms = 1.0/np.sqrt(whdu.data)
    bad = np.where(~np.isfinite(rms))
    rms[bad]=1e31

    # Create a mask
    im = hdul['SCI'].data
    struct = np.ones((10,10))
    mask = np.zeros_like(im, int)
    bad = np.where(np.isnan(im))
    mask[bad] = 1

    print("Creating the edge mask")
    for i in range(4): # Repeat the dilation four times
        mask = binary_dilation(mask, struct)

    to_mask = np.where(mask > 0)
    rms[to_mask] = 1e31
    whdu.data = rms

    detect_hdul = fits.HDUList([hdul[0], hdul['SCI']])

    rms_hdul = fits.HDUList([hdul[0], whdu])


    detect_hdul.writeto('detect.fits', overwrite=True)
    rms_hdul.writeto('rms.fits', overwrite=True)

ss.make_default_setup_file(primary_header, image_header, outfile='default.sex',
                               psfex=args.psfex, withpsf=args.withpsf)
ss.make_default_nnw_file()
ss.make_default_param_file(psfex=args.psfex, withpsf=args.withpsf)
ss.make_default_conv_file()


                        
