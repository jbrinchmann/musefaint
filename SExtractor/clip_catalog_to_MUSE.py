#!/usr/bin/env python

"""Clip a SExtractor catalogue to only have entries within FoV of MUSE 

The photometric catalogues are normally created using HST images as
the detection file. This is optimal for detection but the HST FoV is
usually much larger than that of the MUSE data so we need to cut.
"""

import sys
from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
from astropy.wcs import WCS
from astropy.coordinates import SkyCoord
from astropy.table import Table

import argparse

def get_image(fname):
    hdul = fits.open(fname)
    im = hdul['DATA'].data
    try:
        var_im = hdul['STAT'].data
    except:
        var_im = im*0-9999.
    wcs = WCS(hdul['DATA'])
    header = hdul['DATA'].header
    primary_header = hdul['PRIMARY'].header
    hdul.close()

    return {'im': im, 'var_im': var_im, 'data_header': header,
                'primary_header': primary_header,
                'wcs': wcs}

def get_expimage(expim_fname):
    hdul = fits.open(expim_fname)
    expim = hdul['DATA'].data
    hdr = hdul['DATA'].header
    hdul.close()
    return {'expim': expim, 'expimhdr': hdr}


def load_cat(catname):

    try:
        t = Table().read(catname)
    except ValueError:
        # This happens sometimes when using SExtractor catalogues which
        # contain multiple catalogues.
        try:
            t = Table().read(catname, table_id="Source_List")
        except:
            raise
    except:
        raise
        
    return t

def extract_ra_dec(t):
    """Figure out what columns to use for Ra & DEC

    We try 'ra', 'Ra', 'ALPHA2000', 'ALPHA_J2000' for ra
    and 'dec', 'Dec', 'DELTA2000', 'DELTA_J2000' for dec

    Note that we do not try to figure out the units in the 
    catalogue - the responsibility of this falls on the user.
    """

    to_try_ra  = ['ra', 'Ra', 'ALPHA2000', 'ALPHA_J2000',
                  'ALPHA_J2000_F606W', 'ALPHA_J2000_F814W',
                  '_RAJ2000', 'RA_ICRS']
    to_try_dec = ['dec', 'Dec', 'DELTA2000', 'DELTA_J2000',
                  'DELTA_J2000_F606W', 'DELTA_J2000_F814W',
                  '_DEJ2000', 'DE_ICRS']

    ra_key = None
    for k_ra in to_try_ra:
#        print("Looking for {0}".format(k_ra))
        try:
            ra = t[k_ra]
            ra_key = k_ra
        except:
            pass

    dec_key = None
    for k_dec in to_try_dec:
#        print("Looking for {0}".format(k_dec))
        try:
            dec = t[k_dec]
            dec_key = k_dec
        except:
            pass

    if ra_key is None:
        print("No Ra key was found!")
        ra = None
    else:
        ra = t[ra_key]
        
    if dec_key is None:
        print("No Dec key was found!")
        dec = None
    else:
        dec = t[dec_key]

    return ra, dec

        
def extract_good_cat(cat, d_im, expim=None, explimit=1,
                         border=0):
    """Find parts that we want to avoid"""
    
    # Get ra, dec
    ra, dec = extract_ra_dec(cat)
    
    if (ra is None) | (dec is None):
        print("I could not parse the catalogue columns!")
        return None

    c = SkyCoord(ra=ra, dec=dec, unit="deg")
    # Calculate the x & y coordinates:
    # Does the routine really return this reversed?? What a silly
    # convention - but it does give the right results.
    ypos, xpos = d_im['wcs'].world_to_pixel(c)

    # Which are within the image?
    nx, ny = d_im['im'].shape
    keep_flag = ((xpos >= 0+border) & (xpos < nx-1-border)  &
                     (ypos >= 0+border) & (ypos < ny-1-border))
    keep, = np.where(keep_flag)

    # If the user gave an exposure image then we will use this to
    # further limit the sample
    if expim is not None:
        n_exp = expim[xpos[keep].astype(int), ypos[keep].astype(int)]
        ok, = np.where(n_exp > explimit)
        keep = keep[ok]


    
    good_cat = cat[keep]

        
    return good_cat

    

# Setup the parsing of the command line.
desc = """Subset a catalogue

This script takes a catalogue and an image (at minimum) as input and 
determines which objects in the catalogue are inside the image. This 
set of objects inside the image are then written out to another catalogue.

This operation can be modified by submitting modifiers on the command
line.

"""
parser = argparse.ArgumentParser(description=desc)
parser.add_argument('catalogue', type=str, 
                    help='The name of the file with the catalogue')
parser.add_argument('image', type=str, 
                    help='The name of the file with the image')
parser.add_argument('output', type=str, help="The name of the output catalogue")
parser.add_argument('--format', type=str, help="The format of the output catalogue if not automatically recognised")
parser.add_argument('--border', default=0, type=int,
                        help='The border around the image to exclude')
parser.add_argument('--explimit', default=1, type=int,
                        help='The cut to apply to an exposure image')
parser.add_argument('--expim', default='N/A',
                        help='This can be used to provide an exposure image as input')


args = parser.parse_args()

# Load the relevant files and cut.
d_im = get_image(args.image)
cat = load_cat(args.catalogue)
print(args.output)
if (args.expim != 'N/A'):
    d_exp = get_expimage(args.expim)
else:
    d_exp = {'expim': None}

cat_good = extract_good_cat(cat, d_im, expim=d_exp['expim'], explimit=args.explimit,
                                border=args.border)

cat_good.write(args.output, overwrite=True, format=args.format)
                                
