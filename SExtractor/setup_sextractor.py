"""Create simple SExtractor files for the UFD runs"""

import numpy as np
import os.path

def make_default_conv_file(outfile='gauss_3.0_7x7.conv'):
    """Create a default convolution file for SExtractor

    The standard is to use the 3 pixel Gaussian
    (=0.6" for our reduction) with a 7x7 pixel box.
    """

    txt = """CONV NORM
# 7x7 convolution mask of a gaussian PSF with FWHM = 3.0 pixels.
0.004963 0.021388 0.051328 0.068707 0.051328 0.021388 0.004963
0.021388 0.092163 0.221178 0.296069 0.221178 0.092163 0.021388
0.051328 0.221178 0.530797 0.710525 0.530797 0.221178 0.051328
0.068707 0.296069 0.710525 0.951108 0.710525 0.296069 0.068707
0.051328 0.221178 0.530797 0.710525 0.530797 0.221178 0.051328
0.021388 0.092163 0.221178 0.296069 0.221178 0.092163 0.021388
0.004963 0.021388 0.051328 0.068707 0.051328 0.021388 0.004963
"""

    with open(outfile, "w") as fh:
        fh.write(txt)
        fh.close()


def make_default_nnw_file(outfile='default.nnw'):

    txt = """NNW
# Neural Network Weights for the SExtractor star/galaxy classifier (V1.3)
# inputs:       9 for profile parameters + 1 for seeing.
# outputs:      ``Stellarity index'' (0.0 to 1.0)
# Seeing FWHM range: from 0.025 to 5.5'' (images must have 1.5 < FWHM < 5 pixels)
# Optimized for Moffat profiles with 2<= beta <= 4.

 3 10 10  1

-1.56604e+00 -2.48265e+00 -1.44564e+00 -1.24675e+00 -9.44913e-01 -5.22453e-01  4.61342e-02  8.31957e-01  2.15505e+00  2.64769e-01
 3.03477e+00  2.69561e+00  3.16188e+00  3.34497e+00  3.51885e+00  3.65570e+00  3.74856e+00  3.84541e+00  4.22811e+00  3.27734e+00

-3.22480e-01 -2.12804e+00  6.50750e-01 -1.11242e+00 -1.40683e+00 -1.55944e+00 -1.84558e+00 -1.18946e-01  5.52395e-01 -4.36564e-01 -5.30052e+00
 4.62594e-01 -3.29127e+00  1.10950e+00 -6.01857e-01  1.29492e-01  1.42290e+00  2.90741e+00  2.44058e+00 -9.19118e-01  8.42851e-01 -4.69824e+00
-2.57424e+00  8.96469e-01  8.34775e-01  2.18845e+00  2.46526e+00  8.60878e-02 -6.88080e-01 -1.33623e-02  9.30403e-02  1.64942e+00 -1.01231e+00
 4.81041e+00  1.53747e+00 -1.12216e+00 -3.16008e+00 -1.67404e+00 -1.75767e+00 -1.29310e+00  5.59549e-01  8.08468e-01 -1.01592e-02 -7.54052e+00
 1.01933e+01 -2.09484e+01 -1.07426e+00  9.87912e-01  6.05210e-01 -6.04535e-02 -5.87826e-01 -7.94117e-01 -4.89190e-01 -8.12710e-02 -2.07067e+01
-5.31793e+00  7.94240e+00 -4.64165e+00 -4.37436e+00 -1.55417e+00  7.54368e-01  1.09608e+00  1.45967e+00  1.62946e+00 -1.01301e+00  1.13514e-01
 2.20336e-01  1.70056e+00 -5.20105e-01 -4.28330e-01  1.57258e-03 -3.36502e-01 -8.18568e-02 -7.16163e+00  8.23195e+00 -1.71561e-02 -1.13749e+01
 3.75075e+00  7.25399e+00 -1.75325e+00 -2.68814e+00 -3.71128e+00 -4.62933e+00 -2.13747e+00 -1.89186e-01  1.29122e+00 -7.49380e-01  6.71712e-01
-8.41923e-01  4.64997e+00  5.65808e-01 -3.08277e-01 -1.01687e+00  1.73127e-01 -8.92130e-01  1.89044e+00 -2.75543e-01 -7.72828e-01  5.36745e-01
-3.65598e+00  7.56997e+00 -3.76373e+00 -1.74542e+00 -1.37540e-01 -5.55400e-01 -1.59195e-01  1.27910e-01  1.91906e+00  1.42119e+00 -4.35502e+00

-1.70059e+00 -3.65695e+00  1.22367e+00 -5.74367e-01 -3.29571e+00  2.46316e+00  5.22353e+00  2.42038e+00  1.22919e+00 -9.22250e-01 -2.32028e+00


 0.00000e+00 
 1.00000e+00 
"""
    
    with open(outfile, "w") as fh:
        fh.write(txt)
        fh.close()

    
def make_default_param_file(outfile='default.param', psfex=False, withpsf=False):
    """Create a parameter file for SExtractor"""


    # This is a sneaky way to uncomment options needed for
    # psfphotometry
    if withpsf:
        psfphot_comment = ''
    else:
        psfphot_comment = '#'
    

    txt = """NUMBER                   Running object number                                    
FLUX_APER[1]           
FLUXERR_APER[1]        
MAG_APER[1]            
MAGERR_APER[1]         
FLUX_AUTO                Flux within a Kron-like elliptical aperture               [count]
FLUXERR_AUTO             RMS error for AUTO flux                                   [count]
MAG_AUTO                 Kron-like elliptical aperture magnitude                   [mag]
MAGERR_AUTO              RMS error for AUTO magnitude                              [mag]
FLUX_PETRO               Flux within a Petrosian-like elliptical aperture          [count]
FLUXERR_PETRO            RMS error for PETROsian flux                              [count]
MAG_PETRO                Petrosian-like elliptical aperture magnitude              [mag]
MAGERR_PETRO             RMS error for PETROsian magnitude                         [mag]
FLUX_BEST                Best of FLUX_AUTO and FLUX_ISOCOR                         [count]
FLUXERR_BEST             RMS error for BEST flux                                   [count]
MAG_BEST                 Best of MAG_AUTO and MAG_ISOCOR                           [mag]
MAGERR_BEST              RMS error for MAG_BEST                                    [mag]
SNR_WIN                  Gaussian-weighted SNR                                    
KRON_RADIUS              Kron apertures in units of A or B                        
PETRO_RADIUS             Petrosian apertures in units of A or B                   
ISOAREA_IMAGE            Isophotal area above Analysis threshold                   [pixel**2]
ISOAREAF_IMAGE           Isophotal area (filtered) above Detection threshold       [pixel**2]
XMIN_IMAGE               Minimum x-coordinate among detected pixels                [pixel]
YMIN_IMAGE               Minimum y-coordinate among detected pixels                [pixel]
XMAX_IMAGE               Maximum x-coordinate among detected pixels                [pixel]
YMAX_IMAGE               Maximum y-coordinate among detected pixels                [pixel]
XPEAK_IMAGE              x-coordinate of the brightest pixel                       [pixel]
YPEAK_IMAGE              y-coordinate of the brightest pixel                       [pixel]
ALPHAPEAK_J2000          Right ascension of brightest pix (J2000)                  [deg]
DELTAPEAK_J2000          Declination of brightest pix (J2000)                      [deg]
X_IMAGE                  Object position along x                                   [pixel]
Y_IMAGE                  Object position along y                                   [pixel]
ALPHA_J2000              Right ascension of barycenter (J2000)                     [deg]
DELTA_J2000              Declination of barycenter (J2000)                         [deg]
A_IMAGE                  Profile RMS along major axis                              [pixel]
B_IMAGE                  Profile RMS along minor axis                              [pixel]
THETA_IMAGE              Position angle (CCW/x)                                    [deg]
A_WORLD                  Profile RMS along major axis (world units)                [deg]
B_WORLD                  Profile RMS along minor axis (world units)                [deg]
THETA_WORLD              Position angle (CCW/world-x)                              [deg]
THETA_J2000              Position angle (east of north) (J2000)                    [deg]
MU_THRESHOLD             Analysis threshold above background                       [mag * arcsec**(-2)]
MU_MAX                   Peak surface brightness above background                  [mag * arcsec**(-2)]
FLAGS                    Extraction flags                                         
FWHM_IMAGE               FWHM assuming a gaussian core                             [pixel]
FWHM_WORLD               FWHM assuming a gaussian core                             [deg]
ELONGATION               A_IMAGE/B_IMAGE                                          
ELLIPTICITY              1 - B_IMAGE/A_IMAGE                                      
CLASS_STAR               S/G classifier output                                    
#VECTOR_ASSOC             ASSOCiated parameter vector                              
#NUMBER_ASSOC             Number of ASSOCiated IDs                                 
#THRESHOLDMAX             Maximum threshold possible for detection                  [count]
#FLUX_GROWTH              Cumulated growth-curve                                    [count]
#FLUX_GROWTHSTEP          Step for growth-curves                                    [pixel]
#MAG_GROWTH               Cumulated magnitude growth-curve                          [mag]
#MAG_GROWTHSTEP           Step for growth-curves                                    [pixel]
FLUX_RADIUS(1)
{0}FWHMPSF_IMAGE            FWHM of the local PSF model                               [pixel]
{0}FWHMPSF_WORLD            FWHM of the local PSF model (world units)                 [deg]
{0}XPSF_IMAGE               X coordinate from PSF-fitting                             [pixel]
{0}YPSF_IMAGE               Y coordinate from PSF-fitting                             [pixel]
{0}XPSF_WORLD               PSF position along world x axis                           [deg]
{0}YPSF_WORLD               PSF position along world y axis                           [deg]
{0}ALPHAPSF_SKY             Right ascension of the fitted PSF (native)                [deg]
{0}DELTAPSF_SKY             Declination of the fitted PSF (native)                    [deg]
{0}ALPHAPSF_J2000           Right ascension of the fitted PSF (J2000)                 [deg]
{0}DELTAPSF_J2000           Declination of the fitted PSF (J2000)                     [deg]
{0}ALPHAPSF_B1950           Right ascension of the fitted PSF (B1950)                 [deg]
{0}DELTAPSF_B1950           Declination of the fitted PSF (B1950)                     [deg]
{0}FLUX_PSF                 Flux from PSF-fitting                                     [count]
{0}FLUXERR_PSF              RMS flux error for PSF-fitting                            [count]
{0}MAG_PSF                  Magnitude from PSF-fitting                                [mag]
{0}MAGERR_PSF               RMS magnitude error from PSF-fitting                      [mag]
{0}NITER_PSF                Number of iterations for PSF-fitting                     
{0}CHI2_PSF                 Reduced chi2 from PSF-fitting                            
#ERRX2PSF_IMAGE           Variance of PSF position along x                          [pixel**2]
#ERRY2PSF_IMAGE           Variance of PSF position along y                          [pixel**2]
#ERRXYPSF_IMAGE           Covariance of PSF position between x and y                [pixel**2]
#ERRX2PSF_WORLD           Variance of PSF position along X-WORLD (alpha)            [deg**2]
#ERRY2PSF_WORLD           Variance of PSF position along Y-WORLD (delta)            [deg**2]
#ERRXYPSF_WORLD           Covariance of PSF position X-WORLD/Y-WORLD                [deg**2]
#ERRCXXPSF_IMAGE          Cxx PSF error ellipse parameter                           [pixel**(-2)]
#ERRCYYPSF_IMAGE          Cyy PSF error ellipse parameter                           [pixel**(-2)]
#ERRCXYPSF_IMAGE          Cxy PSF error ellipse parameter                           [pixel**(-2)]
#ERRCXXPSF_WORLD          Cxx PSF error ellipse parameter (WORLD units)             [deg**(-2)]
#ERRCYYPSF_WORLD          Cyy PSF error ellipse parameter (WORLD units)             [deg**(-2)]
#ERRCXYPSF_WORLD          Cxy PSF error ellipse parameter (WORLD units)             [deg**(-2)]
#ERRAPSF_IMAGE            PSF RMS position error along major axis                   [pixel]
#ERRBPSF_IMAGE            PSF RMS position error along minor axis                   [pixel]
#ERRTHETAPSF_IMAGE        PSF error ellipse position angle (CCW/x)                  [deg]
#ERRAPSF_WORLD            World PSF RMS position error along major axis             [pixel]
#ERRBPSF_WORLD            World PSF RMS position error along minor axis             [pixel]
#ERRTHETAPSF_WORLD        PSF error ellipse pos. angle (CCW/world-x)                [deg]
#ERRTHETAPSF_SKY          Native PSF error ellipse pos. angle (east of north)       [deg]
#ERRTHETAPSF_J2000        J2000 PSF error ellipse pos. angle (east of north)        [deg]
#ERRTHETAPSF_B1950        B1950 PSF error ellipse pos. angle (east of north)        [deg]
#DURATION_ANALYSIS        Duration of analysis for this source                      [s]
#VECTOR_MODEL             Model-fitting coefficients                               
#VECTOR_MODELERR          Model-fitting coefficient uncertainties                  
#MATRIX_MODELERR          Model-fitting covariance matrix                          
{0}CHI2_MODEL               Reduced Chi2 of the fit                                  
{0}FLAGS_MODEL              Model-fitting flags                                      
#NITER_MODEL              Number of iterations for model-fitting                   
{0}FLUX_MODEL               Flux from model-fitting                                   [count]
{0}FLUXERR_MODEL            RMS error on model-fitting flux                           [count]
{0}MAG_MODEL                Magnitude from model-fitting                              [mag]
{0}MAGERR_MODEL             RMS error on model-fitting magnitude                      [mag]
#FLUX_HYBRID              Hybrid flux from model-fitting                            [count]
#FLUXERR_HYBRID           RMS error on hybrid flux                                  [count]
#MAG_HYBRID               Hybrid magnitude from model-fitting                       [mag]
#MAGERR_HYBRID            RMS error on hybrid magnitude                             [mag]
{0}FLUX_MAX_MODEL           Peak model flux above background                          [count]
{0}FLUX_EFF_MODEL           Effective model flux above background                     [count]
{0}FLUX_MEAN_MODEL          Mean effective model flux above background                [count]
{0}MU_MAX_MODEL             Peak model surface brightness above background            [mag * arcsec**(-2)]
{0}MU_EFF_MODEL             Effective model surface brightness above background       [mag * arcsec**(-2)]
{0}MU_MEAN_MODEL            Mean effective model surface brightness above background  [mag * arcsec**(-2)]
{0}XMODEL_IMAGE             X coordinate from model-fitting                           [pixel]
{0}YMODEL_IMAGE             Y coordinate from model-fitting                           [pixel]
#XFOCAL_WORLD             Fitted position along focal-plane x axis                 
#YFOCAL_WORLD             Fitted position along focal-plane y axis                 
#XMODEL_WORLD             Fitted position along world x axis                        [deg]
#YMODEL_WORLD             Fitted position along world y axis                        [deg]
#ALPHAMODEL_SKY           Fitted position along right ascension  (native)           [deg]
#DELTAMODEL_SKY           Fitted position along declination (native)                [deg]
#ALPHAMODEL_J2000         Fitted position along right ascension (J2000)             [deg]
#DELTAMODEL_J2000         Fitted position along declination (J2000)                 [deg]
#ALPHAMODEL_B1950         Fitted position along right ascension (B1950)             [deg]
#DELTAMODEL_B1950         Fitted position along declination (B1950)                 [deg]
#ERRX2MODEL_IMAGE         Variance of fitted position along x                       [pixel**2]
#ERRY2MODEL_IMAGE         Variance of fitted position along y                       [pixel**2]
#ERRXYMODEL_IMAGE         Covariance of fitted position between x and y             [pixel**2]
#ERRX2MODEL_WORLD         Variance of fitted position along X-WORLD (alpha)         [deg**2]
#ERRY2MODEL_WORLD         Variance of fitted position along Y-WORLD (delta)         [deg**2]
#ERRXYMODEL_WORLD         Covariance of fitted position X-WORLD/Y-WORLD             [deg**2]
#ERRCXXMODEL_IMAGE        Cxx error ellipse parameter of fitted position            [pixel**(-2)]
#ERRCYYMODEL_IMAGE        Cyy error ellipse parameter of fitted position            [pixel**(-2)]
#ERRCXYMODEL_IMAGE        Cxy error ellipse parameter of fitted position            [pixel**(-2)]
#ERRCXXMODEL_WORLD        Cxx fitted error ellipse parameter (WORLD units)          [deg**(-2)]
#ERRCYYMODEL_WORLD        Cyy fitted error ellipse parameter (WORLD units)          [deg**(-2)]
#ERRCXYMODEL_WORLD        Cxy fitted error ellipse parameter (WORLD units)          [deg**(-2)]
#ERRAMODEL_IMAGE          RMS error of fitted position along major axis             [pixel]
#ERRBMODEL_IMAGE          RMS error of fitted position along minor axis             [pixel]
#ERRTHETAMODEL_IMAGE      Error ellipse pos.angle of fitted position (CCW/x)        [deg]
#ERRAMODEL_WORLD          World RMS error of fitted position along major axis       [deg]
#ERRBMODEL_WORLD          World RMS error of fitted position along minor axis       [deg]
#ERRTHETAMODEL_WORLD      Error ellipse pos.angle of fitted position (CCW/world-x)  [deg]
#ERRTHETAMODEL_SKY        Native fitted error ellipse pos. angle (east of north)    [deg]
#ERRTHETAMODEL_J2000      J2000 fitted error ellipse pos. angle (east of north)     [deg]
#ERRTHETAMODEL_B1950      B1950 fitted error ellipse pos. angle (east of north)     [deg]
#X2MODEL_IMAGE            Variance along x from model-fitting                       [pixel**2]
#Y2MODEL_IMAGE            Variance along y from model-fitting                       [pixel**2]
#XYMODEL_IMAGE            Covariance between x and y from model-fitting             [pixel**2]
#ELLIP1MODEL_IMAGE        Ellipticity component from model-fitting                 
#ELLIP2MODEL_IMAGE        Ellipticity component from model-fitting                 
#POLAR1MODEL_IMAGE        Ellipticity component (quadratic) from model-fitting     
#POLAR2MODEL_IMAGE        Ellipticity component (quadratic) from model-fitting     
#ELLIP1ERRMODEL_IMAGE     Ellipticity component std.error from model-fitting       
#ELLIP2ERRMODEL_IMAGE     Ellipticity component std.error from model-fitting       
#ELLIPCORRMODEL_IMAGE     Corr.coeff between ellip.components from model-fitting   
#POLAR1ERRMODEL_IMAGE     Polarisation component std.error from model-fitting      
#POLAR2ERRMODEL_IMAGE     Polarisation component std.error from model-fitting      
#POLARCORRMODEL_IMAGE     Corr.coeff between polar. components from fitting        
#X2MODEL_WORLD            Variance along X-WORLD (alpha) from model-fitting         [deg**2]
#Y2MODEL_WORLD            Variance along Y_WORLD (delta) from model-fitting         [deg**2]
#XYMODEL_WORLD            Covariance between X-WORLD and Y-WORLD from model-fitting [deg**2]
#ELLIP1MODEL_WORLD        Ellipticity component from model-fitting                 
#ELLIP2MODEL_WORLD        Ellipticity component from model-fitting                 
#POLAR1MODEL_WORLD        Polarisation component from model-fitting                
#POLAR2MODEL_WORLD        Polarisation component from model-fitting                
#ELLIP1ERRMODEL_WORLD     Ellipticity component std.error from model-fitting       
#ELLIP2ERRMODEL_WORLD     Ellipticity component std.error from model-fitting       
#ELLIPCORRMODEL_WORLD     Corr.coeff between ellip.components from model-fitting   
#POLAR1ERRMODEL_WORLD     Polarisation component std.error from model-fitting      
#POLAR2ERRMODEL_WORLD     Polarisation component std.error from model-fitting      
#POLARCORRMODEL_WORLD     Corr.coeff between polar. components from fitting        
#CXXMODEL_IMAGE           Cxx ellipse parameter from model-fitting                  [pixel**(-2)]
#CYYMODEL_IMAGE           Cyy ellipse parameter from model-fittinh                  [pixel**(-2)]
#CXYMODEL_IMAGE           Cxy ellipse parameter from model-fitting                  [pixel**(-2)]
#CXXMODEL_WORLD           Cxx ellipse parameter (WORLD) from model-fitting          [deg**(-2)]
#CYYMODEL_WORLD           Cyy ellipse parameter (WORLD) from model-fitting          [deg**(-2)]
#CXYMODEL_WORLD           Cxy ellipse parameter (WORLD) from model-fitting          [deg**(-2)]
{0}AMODEL_IMAGE             Model RMS along major axis                                [pixel]
{0}BMODEL_IMAGE             Model RMS along minor axis                                [pixel]
{0}THETAMODEL_IMAGE         Model position angle (CCW/x)                              [deg]
{0}AMODEL_WORLD             Model RMS along major axis (WORLD units)                  [deg]
{0}BMODEL_WORLD             Model RMS along minor axis (WORLD units)                  [deg]
{0}THETAMODEL_WORLD         Model position angle (CCW/WORLD-x)                        [deg]
{0}THETAMODEL_SKY           Model position angle (east of north) (native)             [deg]
{0}THETAMODEL_J2000         Model position angle (east of north) (J2000)              [deg]
#THETAMODEL_B1950         Model position angle (east of north) (B1950)              [deg]
{0}SPREAD_MODEL             Spread parameter from model-fitting                      
{0}SPREADERR_MODEL          Spread parameter error from model-fitting                
{0}NOISEAREA_MODEL          Equivalent noise area of the fitted model                 [pixel**2]
#FLUX_BACKOFFSET          Background offset from fitting                            [count]
#FLUXERR_BACKOFFSET       RMS error on fitted background offset                     [count]
{0}FLUX_POINTSOURCE         Point source flux from fitting                            [count]
{0}FLUXERR_POINTSOURCE      RMS error on fitted point source total flux               [count]
#FLUXRATIO_POINTSOURCE    Point-source flux-to-total ratio from fitting            
#FLUXRATIOERR_POINTSOURCE RMS error on point-source flux-to-total ratio            
{0}MAG_POINTSOURCE          Point source total magnitude from fitting                 [mag]
{0}MAGERR_POINTSOURCE       RMS error on fitted point source total magnitude          [mag]
{0}FLUX_SPHEROID            Spheroid total flux from fitting                          [count]
{0}FLUXERR_SPHEROID         RMS error on fitted spheroid total flux                   [count]
{0}MAG_SPHEROID             Spheroid total magnitude from fitting                     [mag]
{0}MAGERR_SPHEROID          RMS error on fitted spheroid total magnitude              [mag]
#FLUX_MAX_SPHEROID        Peak spheroid flux above background                       [count]
#FLUX_EFF_SPHEROID        Effective spheroid flux above background                  [count]
#FLUX_MEAN_SPHEROID       Mean effective spheroid flux above background             [count]
{0}MU_MAX_SPHEROID          Peak spheroid surface brightness above background         [mag * arcsec**(-2)]
{0}MU_EFF_SPHEROID          Effective spheroid surface brightness above background    [mag * arcsec**(-2)]
#MU_MEAN_SPHEROID         Mean effective spheroid surface brightness above backgrou [mag * arcsec**(-2)]
#FLUXRATIO_SPHEROID       Spheroid flux-to-total ratio from fitting                
#FLUXRATIOERR_SPHEROID    RMS error on spheroid flux-to-total ratio                
{0}SPHEROID_REFF_IMAGE      Spheroid effective radius from fitting                    [pixel]
{0}SPHEROID_REFFERR_IMAGE   RMS error on fitted spheroid effective radius             [pixel]
{0}SPHEROID_REFF_WORLD      Spheroid effective radius from fitting                    [deg]
{0}SPHEROID_REFFERR_WORLD   RMS error on fitted spheroid effective radius             [deg]
{0}SPHEROID_ASPECT_IMAGE    Spheroid aspect ratio from fitting                       
{0}SPHEROID_ASPECTERR_IMAGE RMS error on fitted spheroid aspect ratio                
{0}SPHEROID_ASPECT_WORLD    Spheroid aspect ratio from fitting                       
{0}SPHEROID_ASPECTERR_WORLD RMS error on fitted spheroid aspect ratio                
{0}SPHEROID_THETA_IMAGE     Spheroid position angle (CCW/x) from fitting              [deg]
{0}SPHEROID_THETAERR_IMAGE  RMS error on spheroid position angle                      [deg]
{0}SPHEROID_THETA_WORLD     Spheroid position angle (CCW/world-x)                     [deg]
{0}SPHEROID_THETAERR_WORLD  RMS error on spheroid position angle                      [deg]
{0}SPHEROID_THETA_SKY       Spheroid position angle (east of north, native)           [deg]
{0}SPHEROID_THETA_J2000     Spheroid position angle (east of north, J2000)            [deg]
#SPHEROID_THETA_B1950     Spheroid position angle (east of north, B1950)            [deg]
{0}SPHEROID_SERSICN         Spheroid Sersic index from fitting                       
{0}SPHEROID_SERSICNERR      RMS error on fitted spheroid Sersic index                
{0}FLUX_DISK                Disk total flux from fitting                              [count]
{0}FLUXERR_DISK             RMS error on fitted disk total flux                       [count]
{0}MAG_DISK                 Disk total magnitude from fitting                         [mag]
{0}MAGERR_DISK              RMS error on fitted disk total magnitude                  [mag]
{0}FLUX_MAX_DISK            Peak disk flux above background                           [count]
{0}FLUX_EFF_DISK            Effective disk flux above background                      [count]
{0}FLUX_MEAN_DISK           Mean effective disk flux above background                 [count]
{0}MU_MAX_DISK              Peak disk surface brightness above background             [mag * arcsec**(-2)]
{0}MU_EFF_DISK              Effective disk surface brightness above background        [mag * arcsec**(-2)]
{0}MU_MEAN_DISK             Mean effective disk surface brightness above background   [mag * arcsec**(-2)]
#FLUXRATIO_DISK           Disk flux-to-total ratio from fitting                    
#FLUXRATIOERR_DISK        RMS error on disk flux-to-total ratio                    
{0}DISK_SCALE_IMAGE         Disk scalelength from fitting                             [pixel]
{0}DISK_SCALEERR_IMAGE      RMS error on fitted disk scalelength                      [pixel]
{0}DISK_SCALE_WORLD         Disk scalelength from fitting (world coords)              [deg]
{0}DISK_SCALEERR_WORLD      RMS error on fitted disk scalelength (world coords)       [deg]
{0}DISK_ASPECT_IMAGE        Disk aspect ratio from fitting                           
{0}DISK_ASPECTERR_IMAGE     RMS error on fitted disk aspect ratio                    
{0}DISK_ASPECT_WORLD        Disk aspect ratio from fitting                           
{0}DISK_ASPECTERR_WORLD     RMS error on disk aspect ratio                           
{0}DISK_INCLINATION         Disk inclination from fitting                             [deg]
{0}DISK_INCLINATIONERR      RMS error on disk inclination from fitting                [deg]
{0}DISK_THETA_IMAGE         Disk position angle (CCW/x) from fitting                  [deg]
{0}DISK_THETAERR_IMAGE      RMS error on fitted disk position angle                   [deg]
{0}DISK_THETA_WORLD         Disk position angle (CCW/world-x)                         [deg]
{0}DISK_THETAERR_WORLD      RMS error on disk position angle                          [deg]
{0}DISK_THETA_SKY           Disk position angle (east of north, native)               [deg]
{0}DISK_THETA_J2000         Disk position angle (east of north, J2000)                [deg]
#DISK_THETA_B1950         Disk position angle (east of north, B1950)                [deg]
#CHI2_DETMODEL            Reduced Chi2 of the det. model fit to measurement image  
#FLAGS_DETMODEL           Detection model-fitting flags                            
#NITER_DETMODEL           Number of iterations for detection model-fitting         
#FLUX_DETMODEL            Flux from detection model-fitting                         [count]
#FLUXERR_DETMODEL         RMS error on detection model-fitting flux                 [count]
#MAG_DETMODEL             Magnitude from detection model-fitting                    [mag]
#MAGERR_DETMODEL          RMS error on detection model-fitting magnitude            [mag]
""".format(psfphot_comment)

    if psfex:
        txt = txt+"VIGNET(45,45)\n"
        txt = txt+"#VIGNET_SHIFT             Pixel data around detection, corrected for shift          [count]\n"
    else:
        txt = txt+"#VIGNET(45,45)\n"
        txt = txt+"#VIGNET_SHIFT             Pixel data around detection, corrected for shift          [count]\n"
        
    with open(outfile, "w") as fh:
        fh.write(txt)
        fh.close()


def make_default_setup_file(phdr, ihdr, outfile='default.sex',
                                output_catalog='detection.vot',
                                phot_aperture=5.0, 
                                parameterfile='default.param',
                                psfex=False, withpsf=False):
    """Create a simple SExtractor setup file for HST images

    This uses very standard settings"""


    catalogtype = 'ASCII_VOTABLE'
    # Check whether the user wants psfex suitable output.
    # If so we need to change the output table version and check
    # the name of the output catalog.
    #
    # See https://psfex.readthedocs.io/en/latest/GettingStarted.html
    #
    # for general guidelines for PSFex
    if (psfex):
        catalogtype = 'FITS_LDAC'
        aperture_arcsec = 1.5
        # Convert aperture to pixels.
        im_scale = np.abs(3600*ihdr['CD1_1']) # arcsec/pixel
        phot_aperture = aperture_arcsec/im_scale 
        root, ext = os.path.splitext(output_catalog)
        if (ext != '.fits') & (ext != '.fit'):
            # Ok, update the name.
            output_catalog = root+'.fits'

        detect_threshold = 1.5
        analysis_threshold = 1.5
    else:
        detect_threshold = 0.6
        analysis_threshold = 0.75
        

    if withpsf:
        # The user will run with a PSF file created by PSFex. This
        # requires some additional information in the config file.
        psfname = 'detection.psf'
        fwhm = 0
    else:
        psfname = 'default.psf'
        fwhm = 0.12
        
    photflam = ihdr['PHOTFLAM']
    photplam = ihdr['PHOTPLAM']
    exptime = phdr['EXPTIME']

    # Gain, following http://www.ifa.hawaii.edu/~rgal/science/sextractor_notes.html
    if 'CCDGAIN' in phdr:
        gain = phdr['CCDGAIN']*exptime
    elif 'ATODGAIN' in phdr:
        gain = phdr['ATODGAIN']*exptime
    else:
        gain = exptime
        print("Gain is not found in header!")

    # AB Zero point.
    abmag_zp = -2.5*np.log10(photflam)-5*np.log10(photplam)-2.408
    

    txt = """
# Simple setup file for SExtractor - created by make_default_setup_file.
#
 
#-------------------------------- Catalog ------------------------------------
 
CATALOG_NAME     {catalog}       # name of the output catalog
CATALOG_TYPE     {cat_type}       # NONE,ASCII,ASCII_HEAD, ASCII_SKYCAT,
                                # ASCII_VOTABLE, FITS_1.0 or FITS_LDAC
PARAMETERS_NAME  {parfile}  # name of the file containing catalog contents
 
#------------------------------- Extraction ----------------------------------
 
DETECT_TYPE      CCD            # CCD (linear) or PHOTO (with gamma correction)
DETECT_MINAREA   5              # min. # of pixels above threshold
DETECT_MAXAREA   0              # max. # of pixels above threshold (0=unlimited)
THRESH_TYPE      RELATIVE       # threshold type: RELATIVE (in sigmas)
                                # or ABSOLUTE (in ADUs)
DETECT_THRESH    {detthr:0.2f}            # <sigmas> or <threshold>,<ZP> in mag.arcsec-2
ANALYSIS_THRESH  {anthr:0.2f}            # <sigmas> or <threshold>,<ZP> in mag.arcsec-2
 
FILTER           Y              # apply filter for detection (Y or N)?
FILTER_NAME      gauss_3.0_7x7.conv    # name of the file containing the filter
FILTER_THRESH                   # Threshold[s] for retina filtering
 
DEBLEND_NTHRESH  32             # Number of deblending sub-thresholds
DEBLEND_MINCONT  0.005          # Minimum contrast parameter for deblending
 
CLEAN            Y              # Clean spurious detections? (Y or N)?
CLEAN_PARAM      1.0            # Cleaning efficiency
 
MASK_TYPE        CORRECT        # type of detection MASKing: can be one of
                                # NONE, BLANK or CORRECT
 
#-------------------------------- WEIGHTing ----------------------------------

WEIGHT_TYPE      MAP_RMS        # type of WEIGHTing: NONE, BACKGROUND,
                                # MAP_RMS, MAP_VAR or MAP_WEIGHT
RESCALE_WEIGHTS  Y              # Rescale input weights/variances (Y/N)?
WEIGHT_IMAGE     rms.fits       # weight-map filename
WEIGHT_GAIN      N              # modulate gain (E/ADU) with weights? (Y/N)
WEIGHT_THRESH                   # weight threshold[s] for bad pixels

#-------------------------------- FLAGging -----------------------------------

FLAG_IMAGE       flag.fits      # filename for an input FLAG-image
FLAG_TYPE        OR             # flag pixel combination: OR, AND, MIN, MAX
                                # or MOST

#------------------------------ Photometry -----------------------------------
 
PHOT_APERTURES   {phot_ap:0.1f}              # MAG_APER aperture diameter(s) in pixels
PHOT_AUTOPARAMS  2.5, 3.5       # MAG_AUTO parameters: <Kron_fact>,<min_radius>
PHOT_PETROPARAMS 2.0, 3.5       # MAG_PETRO parameters: <Petrosian_fact>,
                                # <min_radius>
PHOT_AUTOAPERS   0.0,0.0        # <estimation>,<measurement> minimum apertures
                                # for MAG_AUTO and MAG_PETRO
PHOT_FLUXFRAC    0.5            # flux fraction[s] used for FLUX_RADIUS
 
SATUR_LEVEL      450.0        # level (in ADUs) at which arises saturation
SATUR_KEY        SATURATE       # keyword for saturation level (in ADUs)
 
MAG_ZEROPOINT    {zp:0.5f}            # magnitude zero-point
MAG_GAMMA        4.0            # gamma of emulsion (for photographic scans)
GAIN             {gain:f}           # CCDGAIN*exposure_time - as per http://www.ifa.hawaii.edu/~rgal/science/sextractor_notes.html   
GAIN_KEY         GAIN           # keyword for detector gain in e-/ADU
PIXEL_SCALE      0              # size of pixel in arcsec (0=use FITS WCS info)
 
#------------------------- Star/Galaxy Separation ----------------------------
 
SEEING_FWHM      {fwhm}            # stellar FWHM in arcsec
STARNNW_NAME     default.nnw    # Neural-Network_Weight table filename
 
#------------------------------ Background -----------------------------------
 
BACK_TYPE        AUTO           # AUTO or MANUAL
BACK_VALUE       0.0            # Default background value in MANUAL mode
BACK_SIZE        64             # Background mesh: <size> or <width>,<height>
BACK_FILTERSIZE  3              # Background filter: <size> or <width>,<height>
 
BACKPHOTO_TYPE   GLOBAL         # can be GLOBAL or LOCAL
BACKPHOTO_THICK  24             # thickness of the background LOCAL annulus
BACK_FILTTHRESH  0.0            # Threshold above which the background-
                                # map filter operates
 
#------------------------------ Check Image ----------------------------------
 
CHECKIMAGE_TYPE  -OBJECTS,SEGMENTATION           # can be NONE, BACKGROUND, BACKGROUND_RMS,
                                # MINIBACKGROUND, MINIBACK_RMS, -BACKGROUND,
                                # FILTERED, OBJECTS, -OBJECTS, SEGMENTATION,
                                # or APERTURES
CHECKIMAGE_NAME  noobj.fits,seg.fits     # Filename for the check-image
 
#--------------------- Memory (change with caution!) -------------------------
 
MEMORY_OBJSTACK  3000           # number of objects in stack
MEMORY_PIXSTACK  300000         # number of pixels in stack
MEMORY_BUFSIZE   1024           # number of lines in buffer
 
#------------------------------- ASSOCiation ---------------------------------

ASSOC_NAME       sky.list       # name of the ASCII file to ASSOCiate
ASSOC_DATA       2,3,4          # columns of the data to replicate (0=all)
ASSOC_PARAMS     2,3,4          # columns of xpos,ypos[,mag]
ASSOCCOORD_TYPE  PIXEL          # ASSOC coordinates: PIXEL or WORLD
ASSOC_RADIUS     2.0            # cross-matching radius (pixels)
ASSOC_TYPE       NEAREST        # ASSOCiation method: FIRST, NEAREST, MEAN,
                                # MAG_MEAN, SUM, MAG_SUM, MIN or MAX
ASSOCSELEC_TYPE  MATCHED        # ASSOC selection type: ALL, MATCHED or -MATCHED

#----------------------------- Miscellaneous ---------------------------------
 
VERBOSE_TYPE     NORMAL         # can be QUIET, NORMAL or FULL
HEADER_SUFFIX    .head          # Filename extension for additional headers
WRITE_XML        Y              # Write XML file (Y/N)?
XML_NAME         sex.xml        # Filename for XML output
XSL_URL          file:///usr/share/sextractor/sextractor.xsl
                                # Filename for XSL style-sheet
NTHREADS         1              # 1 single thread

FITS_UNSIGNED    N              # Treat FITS integer values as unsigned (Y/N)?
INTERP_MAXXLAG   16             # Max. lag along X for 0-weight interpolation
INTERP_MAXYLAG   16             # Max. lag along Y for 0-weight interpolation
INTERP_TYPE      ALL            # Interpolation type: NONE, VAR_ONLY or ALL

#--------------------------- Experimental Stuff -----------------------------

PSF_NAME         {psfname}    # File containing the PSF model
PSF_NMAX         1              # Max.number of PSFs fitted simultaneously
PATTERN_TYPE     RINGS-HARMONIC # can RINGS-QUADPOLE, RINGS-OCTOPOLE,
                                # RINGS-HARMONICS or GAUSS-LAGUERRE
SOM_NAME         default.som    # File containing Self-Organizing Map weights

""".format(catalog=output_catalog, cat_type=catalogtype,
               parfile=parameterfile, detthr=detect_threshold,
               anthr=analysis_threshold,
               phot_ap=phot_aperture,
               zp=abmag_zp, gain=gain, fwhm=fwhm, psfname=psfname)

    
    with open(outfile, "w") as fh:
        fh.write(txt)
        fh.close()

