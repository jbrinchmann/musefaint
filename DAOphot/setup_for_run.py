#!/usr/bin/env python

import os
import shutil

def make_daophot_run(fltr, n):
    """Create a daophot run file"""

    # These values could be read from the header but
    # I prefer to have them explicit here
    ncombine = {'F814W': 1, 'F606W': 1, 'F475W': 1}
    
    command = """ATTACH ../HydraII-{0}-HST-{2}.fits
FIND
{1},1
HydraII-{0}
y
PHOT
photo.opt

HydraII-{0}.coo
HydraII-{0}.ap
PICK
HydraII-{0}.ap
30,16
HydraII-{0}.lst
exit
""".format(fltr, ncombine[fltr], n)
    
    fh = open('run_daophot.txt', 'w')
    fh.write(command)
    fh.close()


def make_master_run_files(fltr):

    command = """#!/bin/bash

# Run DAOPhot for the HydraII data.
#
# First run daophot itself.
rm -f HydraII-{0}.coo
rm -f HydraII-{0}.ap
rm -f HydraII-{0}.lst
cat run_daophot.txt | ~/Source/DAOPhot/daophot
    """.format(fltr)
    fh = open('run_daophot.sh', 'w')
    fh.write(command)
    fh.close()

    
    command = """#!/bin/bash
    
# Then determine the PSF.
# It is preferrable to do this manuall so this is commented out by default.
echo "PSF estimation is commented out - run interactively"
rm -f HydraII-{0}.psf
cat run_psf.txt | ~/Source/DAOPhot/daophot
    """.format(fltr)

    fh = open('run_psf.sh', 'w')
    fh.write(command)
    fh.close()

    command = """#!/bin/bash

# Finally run allstar. Again this is commented out by default.
echo "Running allstar"
rm -f HydraII-{0}.als
cat run_allstar.txt | ~/Source/DAOPhot/allstar
    """.format(fltr)

    fh = open('run_allstar.sh', 'w')
    fh.write(command)
    fh.close()

    

def make_psf_run(fltr, n):
    """Create a file for PSF running"""

    command = """
ATTACH HydraII-{0}-sci-blanked-{1}.fits
OPT
WA=1

PSF
HydraII-{0}.ap
HydraII-{0}.lst
HydraII-{0}.psf
EXIT

""".format(fltr, n)
    
    fh = open('run_psf.txt', 'w')
    fh.write(command)
    fh.close()


def make_allstar_run(fltr, n):
    """Create a file for PSF running"""

    command = """

HydraII-{0}-sci-blanked-{1}.fits
HydraII-{0}.psf
HydraII-{0}.ap
HydraII-{0}.als
HydraII-{0}s
""".format(fltr, n)
    
    fh = open('run_allstar.txt', 'w')
    fh.write(command)
    fh.close()




fltrs = ('F475W', 'F606W', 'F814W')
num = (200, 400)
to_copy = ('daophot.opt', 'photo.opt', 'allstar.opt')

for n_blanked in num:
    os.chdir("{0}".format(n_blanked))
    for f in fltrs:
        fname = "HydraII-{0}-sci-blanked-{1}.fits".format(f, n_blanked)
        os.chdir(f)
        try:
            os.symlink('../../../'+fname, fname)
        except OSError:
            # This happens when the link is already made
            pass
        except:
            raise

        # Copy files that might (will) be modified
        for tc in to_copy:
            shutil.copy2('../../'+tc, tc)
    
        make_daophot_run(f, n_blanked)
        make_psf_run(f, n_blanked)
        make_allstar_run(f, n_blanked)
        make_master_run_files(f)
        
        os.chdir('../')

    os.chdir('../')
