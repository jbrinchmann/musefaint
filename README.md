# A repository for various code and information for MUSE-Faint 

This repository is a convenience gathering place for scripts that are
useful for MUSE-Faint. It is not a terribly structured place.

There should be README's in all subdirectories to explain what is there

The most useful are the utilities probably.

### Content###

* SExtractor - scripts to run SExtractor on HST data to create
  photometric catalogues for our analysis
* DAOphot - scripts to run DAOphot on the HST data to create improved
  photometric catalogues.
* Utilities - various additional scripts.
* StarGalaxy - A script for doing star-galaxy separation on a
  catalogue from SExtractor using its PSF fitting routines.
* VdispEstimation - a simple base script for estimating the velocity
  dispersion of an ultra-faint dwarf.
  
  

